""" Entry point for `tsrc checkout` """
import argparse
import os

import attr
from path import Path
import cli_ui as ui

import tsrc
from tsrc.workspace.manifest_config import ManifestConfig

# TODO: add an optional flag that specifies whether to clean up unwanted repos, default false
# the commented code is progress towards this

def main(args: argparse.Namespace) -> None:
    
    ui.info_1("Checking out manifest branch to", ui.bold, args.manifest_branch)
    
    workspace = tsrc.cli.get_workspace(args)
    
    # workspace.load_manifest()
    # before_repos = workspace.get_repos()
    
    workspace.checkout_manifest(args.manifest_branch)
    workspace.load_manifest()
    
    # after_repos = workspace.get_repos()
    # print(before_repos, after_repos)
    
    workspace.clone_missing()
    workspace.checkout_repos()
