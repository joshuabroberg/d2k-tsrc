""" Entry point for `tsrc branch` """
import argparse
import os

import attr
from path import Path
import cli_ui as ui

import tsrc
from tsrc.workspace.manifest_config import ManifestConfig

def main(args: argparse.Namespace) -> None:
    
    workspace = tsrc.cli.get_workspace(args)
    
    branch_args = ["branch"]
    
    if args.all:
        branch_args.append("--all")
    
    if args.remotes:
        branch_args.append("--remotes")
    
    try:
        tsrc.git.run(workspace.local_manifest.clone_path, *branch_args)
    except tsrc.Error:
        raise tsrc.Error("Failed to get branch list")
    