# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* D2K-specific version of TSRC
* Adds `tsrc checkout`, a command to checkout manifest branches and all specified repos at once
* Version 0.0.1

### How do I get set up? ###

* Windows
    * Install Python 3.7 or higher
	* Install pip3
	* Install tsrc (`pip install tsrc`)
	* Clone d2k-tsrc
	* Replace C:\Users\<user>\AppData\Local\Programs\Python\<PythonXX>\Lib\site-packages\tsrc with d2k-tsrc
	* Make sure you have Git 2.7.0 or higher installed
* Centos 7
    * Install Python 3.7 or higher in a new directory (preserving the old installation, as it is needed by yum)
	* Install pip3 for your new Python installation
	* Install tsrc (`pip install tsrc`)
	* Clone d2k-tsrc
	* Replace the tsrc site-package with the d2k-tech repository
	* Make sure you have Git 2.7.0 or higher installed

### How do I use this? ###

Most of the documentation you need is provided by the original developer here:

* https://tankerhq.github.io/tsrc/

To use D2K's additions, simply type:

* `tsrc sync`
* `tsrc checkout <manifest branch> `

### Contribution guidelines ###

* Don't break the build

### Who do I talk to? ###

* Joshua Broberg
* joshua.broberg@d2ktech.com
* You can find the rest of the D2K team at https://www.d2ktech.com