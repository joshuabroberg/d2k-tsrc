from path import Path
import cli_ui as ui

import tsrc
import tsrc.git
import tsrc.executor

class Checkoutter(tsrc.executor.Task[tsrc.Repo]):
    def __init__(self, workspace_path: Path) -> None:
        self.workspace_path = workspace_path

    def on_start(self, *, num_items: int) -> None:
        ui.info_1("Checking out repos")

    def on_failure(self, *, num_errors: int) -> None:
        ui.error("Failed to checkout")

    def display_item(self, repo: tsrc.Repo) -> str:
        ref = "?"
        if repo.tag:
            ref = repo.tag
        elif repo.branch:
            ref = repo.branch
        return repo.src + " -> " + ref

    def process(self, index: int, count: int, repo: tsrc.Repo) -> None:
        repo_desc = self.display_item(repo)
        message = [ui.bold, repo_desc]
        ui.info(*message, sep="")
        self.checkout_repo(repo)
    
    def checkout_repo(self, repo: tsrc.Repo) -> None:
        repo_path = self.workspace_path / repo.src
        if repo.tag:
            ref = repo.tag
        elif repo.branch:
            ref = repo.branch
        if ref:
            ui.info_2("Checking out branch")
            try:
                checkout_args = ["checkout", ref]
                tsrc.git.run(repo_path, *checkout_args)
            except tsrc.Error:
                try:
                    checkout_args = ["checkout", "-b", ref, repo.remotes[0].name + "/" + ref]
                    tsrc.git.run(repo_path, *checkout_args)
                except tsrc.Error:
                    raise tsrc.Error("Checkout failed")
            ui.info_2("Updating branch")
            try:
                tsrc.git.run(repo_path, "merge", "--ff-only", "@{upstream}")
            except tsrc.Error:
                raise tsrc.Error("updating branch failed")
        else:
            raise tsrc.Error("Checkout failed; no branch specified")
